#include "PinNames.h"
#include "ThisThread.h"
#include "mbed.h"

DigitalOut led1(D7);
InterruptIn button1 (BUTTON1);
static auto sleep_time = 1000ms;

Ticker toggle_led_ticker;
void led_ticker()
{
	led1 = !led1;
}
void pressed()
{
	toggle_led_ticker.detach(); // открепляет таймер
	static int count = 0;
	count += 1;
	if (count == 1)
	{
    	led_ticker();
    	count = 0;
	}
	toggle_led_ticker.attach(&led_ticker, sleep_time); // прикрепляет таймер
}
int main()
{
	toggle_led_ticker.attach(&led_ticker, sleep_time);
	button1.rise(&pressed);
	while (true) {
 }
}
