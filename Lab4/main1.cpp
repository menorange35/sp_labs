#include "PinNames.h"
#include "ThisThread.h"
#include "mbed.h"
//igitalOut led1(D7);
DigitalOut led1(D7);
// Объявляем пин кнопки 1 как вход прерывания
InterruptIn button1 (BUTTON1);
// Задержка, 1 секунда == 1000 мс
static auto sleep_time = 1000ms;
Ticker toggle_led_ticker;

void led_ticker()
{
    led1 = !led1;
}

void pressed()
{
    toggle_led_ticker.detach(); // открепляет таймер
    // Управление скоростью мерцания светодиода
    sleep_time += 250ms;
    if (sleep_time > 1000ms)
    sleep_time = 250ms;
    toggle_led_ticker.attach(&led_ticker, sleep_time); // прикрепляет
}

void released()
{
    led1=0;
}
// Основной поток main
int main()
{
    toggle_led_ticker.attach(&led_ticker, sleep_time);
    button1.fall(&released);
    button1.rise(&pressed);
    while (true) {
    // основной суперцикл можно использовать для других задач
    }
} 
