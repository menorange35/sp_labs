#include "mbed.h"
#include "BME280.h"

DigitalOut led1(LED1);
DigitalOut led2(D7);
static int glow_f;
BME280 sensor(D14,D15);
EventQueue eventQueue(/* event count */ 50 * EVENTS_EVENT_SIZE);

void led_glow(int glow_param)
{
    if (glow_param <=28){
        led1 = 1;
        led2=0;
        }
    else
    {
        led1=0;
        led2 = 1;
    }
}

void printTemperature(void)
{
        printf("%d degC, %d hPa, %d %%\n", (int)sensor.getTemperature(), (int)sensor.getPressure(), (int)sensor.getHumidity());
        glow_f = (int)sensor.getTemperature(); //исследование параметра температуры
        led_glow(glow_f);
}

// Основной поток main
int main()
{
    eventQueue.call_every(1000, printTemperature); // старт каждые 1000 мс
    eventQueue.dispatch_forever();
    
    return 0;
}
