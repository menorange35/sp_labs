#!/bin/bash

echo "Repin Danil, 718-1"
echo -e "\n----- WORK WITH USERS -----\n"

while true
do
    # Запрос имени пользователя
    echo ""
    read -p "Enter user name: " user_name
    if [ "$user_name" = "" ]
    then
        echo -e "ERROR! Empty name\n"
        continue
    fi

    # Поиск пользователя в списках
    user=`cat /etc/shadow | grep -w "$user_name"`

    # Если такой пользователь не найден
    if [ "$user" = "" ]
    then
        echo -e "ERROR! User not found\n"
        continue
    fi


    echo "1) Unlock user"
    echo "2) Lock user"
    echo "0) Exit"

    read -p "Select action: " select

    if [ "$select" = "1" ]
    then
        passwd -u $user_name 1>tmp.txt
    elif [ "$select" = "2" ]
    then
        passwd -l $user_name 1>tmp.txt
    elif [ "$select" = "0" ]
    then
        break
    else
        echo "ERROR! Uncorrect input\n"
    fi

    echo ""
    read -p "Retry [y/n]: " retry
    if [ "$retry" = "y" ]
    then
        continue
    fi

    break

done

