# 19 вариант
#
# Репин Данил, 718-1
# Сумма элементов массива (байт), двоичный код которых содержит 1 в битах
# 2 и 6.
# ==========================================

# Блок с данными
.data

# Массив из 10 байт
array:
    .byte 8, 5, 0, 4, 12, 54, 23, 108, 156, 254
array_end:

# Для вывода числа массива
format_num:
    .string "%d "

# Для вывод суммы 
format_sum:
    .string "\nSum = %d\n"

# Блок с кодом
.text
.globl main

# Начало программы
main:
    # Обнуление регистра суммы
    xorl %esi, %esi
    # Обнуление промежуточного регистра, куда будем складывать эл. из массива
    xorl %ecx, %ecx
    # Кидаем в регистр адрес начала массива
    movl $array, %ebx
    # Кидаем маску со 2 и 6 битом
    movb $0b01000100, %dl

check_num:


    movb (%ebx), %cl
    # Применение маски
    andb %dl, %cl
    cmpb %cl, %dl
    # Если биты не установлены, то переход к след. эл. массива
    jne next_num

    # Снова получаем этот эл.
    movb (%ebx), %cl
    # Суммируем его с суммой
    addl %ecx, %esi

next_num:
    # Увеличение адреса массива
    inc %ebx
    # Если он не дошел до конца, то повторяем действия
    cmpl $array_end, %ebx
    jne check_num


    # Кидаем в регистр адрес начала массива
    movl $array, %ebx
    # Обнуление промежуточного регистра, куда будем складывать эл. из массива
    xorl %ecx, %ecx
print_array:
    xorl %ecx, %ecx
    # Получаем эл из массива
    movb (%ebx), %cl

    # Выводим этот элемент
    pushl %ecx
    pushl $format_num
    call printf
    # Возвращаем стэк обратно (push 2 раза = 2 * 4, 4 байта = 32 бита, а это разрядность проги)
    addl $8, %esp

    inc %ebx
    cmpl $array_end, %ebx
    jne print_array


    # Выводим сумму
    pushl %esi
    pushl $format_sum
    call printf
    addl $8, %esp

    # Завершаем программу
    movl $1, %eax
    movl $0, %ebx
    xorl %ecx, %ecx
    xorl %edx, %edx
    int $0x80


