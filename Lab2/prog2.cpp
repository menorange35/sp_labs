#include <iostream>

using namespace std;

int main()
{
    // Массив байт
    unsigned char array[] = {8, 5, 0, 4, 12, 54, 23, 108, 156, 254};
    // Маска со 2 и 6 битом
    unsigned char mask = 0x44;

    // Итоговая сумма
    int sum = 0;

    // Проход по каждому элементу 
    for (int i = 0; i < 10; ++i)
    {
        // Вывод элемента
        cout << int(array[i]) << " ";

        // Проверка числа по маске
        unsigned char tmp = array[i] & mask;
        // Если маска подходит, то суммируем 
        if (tmp == mask)
            sum += array[i];
    }
    cout << endl;

    // Вывод суммы
    cout << "Sum = " << sum << endl;

    return 0;
}
