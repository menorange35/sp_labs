# Блок с данными
.data 

hello:
    .string "hello world!\n"
    .set len, . - hello - 1


# Блок с кодом
.text
.globl main

main:
    # Кидаем в eax и ebx код вывода на консоль
    movl $4, %eax
    movl $1, %ebx
    # Кидаем указатель на начало строки и ее длину
    movl $hello, %ecx
    movl $len, %edx
    # Вызываем вывод
    int $0x80

    # Кидаем в eax код завершения программы
    movl $1, %eax
    # Кидаем в ebx код, возвращаемый прогой
    # аналагом в С++ будет return 0
    movl $0, %ebx
    # Вызываем завершение
    int $0x80
