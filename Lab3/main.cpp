#include <iostream>

using namespace std;

int main()
{
    // Для рандомной генерации
    srand(time(NULL));

    // Максимальное число в массиве
    const int MAX_NUM = 20;

    // Ввод размера двух массивов
    int size;
    cout << "Enter size array: ";
    cin >> size;

    // Выделение памяти под массивы
    int* a1 = new int[size];
    int* a2 = new int[size];
    int* res = new int[size * 2];

    // Случайное заполнение массивов
    for (int i = 0; i < size; ++i)
    {
        a1[i] = rand() % MAX_NUM;
        a2[i] = rand() % MAX_NUM;
    }

    // Вывод первого массива
    cout << "Array #1: ";
    for (int i = 0; i < size; ++i)
        cout << a1[i] << "  ";
    cout << endl;

    // Вывод второго массива
    cout << "Array #2: ";
    for (int i = 0; i < size; ++i)
        cout << a2[i] << "  ";
    cout << endl;

    asm(
            // Кидаем в регистры основные переменные из проги
            // ВАЖНО! Кидаются не просто массивы, а указатели на них
            // (указатели на первые элементы массивов)
            "movq %[a1], %%rax;"
            "movq %[a2], %%rbx;"
            "movl %[size], %%ecx;"
            "movq %[res], %%rdx;"
            
        "mov_el:"
            // Перемещаем элемент из первого массива в итоговый
            "movl (%%rax), %%esi;"
            "movl %%esi, (%%rdx);"
            // Смещаемся в этих двух массивах
            "addq $4, %%rax;"
            "addq $4, %%rdx;"

            // Перемещаем элемент из второго массива в итоговый
            "movl (%%rbx), %%esi;"
            "movl %%esi, (%%rdx);"
            // Смещаемся в этих массивах
            "addq $4, %%rbx;"
            "addq $4, %%rdx;"
            // Стоит заметить, что в результируещем массиве сместились дважды,
            // в результате чего элементы будут по очереди заполняться

            // Выполняем этий действия ecx раз (size)
            "loop mov_el;"
        :
        : [a1]"m"(a1), [a2]"m"(a2), [size]"m"(size), [res]"m"(res)
        :
    );

    // Вывод итогового массива
    cout << "Array #(1+2): ";
    for (int i = 0; i < size * 2; ++i)
        cout << res[i] << "  ";
    cout << endl;


    return 0;
}
